import unittest
from main import *

class MainTest(unittest.TestCase):
    def test_otter(self):
        o = Otter("John")
        assert o.sound() == "I am a Otter. And I am a Fish."
        assert o.swim() == "John is swimming."
        assert o.walk() == "John is walking."

    def test_mouse(self):
        o = Mouse("Lilly")
        assert o.sound() == "I am a Mouse. And I am a Mammal."
        assert o.walk() == "Lilly is walking."

    def test_whale(self):
        o = Whale("Bob")
        assert o.sound() == "I am a Whale. And I am a Fish."
        assert o.swim() == "Bob is swimming."

    def test_catfish(self):
        o = Catfish("George")
        assert o.sound() == "I am a Catfish. And I am a Fish."
        assert o.swim() == "George is swimming."