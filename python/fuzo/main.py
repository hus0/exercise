
class Animal:
    def __init__(self, name, species):
        self.name = name
        self.species = species

class Mammal(Animal):
    def __init__(self, name, species):
        super().__init__(name, species)

    def sound(self):
        return "I am a %s. And I am a Mammal." %self.species

class Fish(Animal):
    def __init__(self, name, species):
        super().__init__(name, species)

    def sound(self):
        return "I am a %s. And I am a Fish." %self.species

class Walker(Animal):
    def walk(self):
        return "%s is walking." %self.name

class Swimmer(Animal):
    def swim(self):
        return "%s is swimming." %self.name

class Mouse(Mammal, Walker):
    def __init__(self, name):
        super().__init__(name, self.__class__.__name__)

class Whale(Fish, Swimmer):
    def __init__(self, name):
        super().__init__(name, self.__class__.__name__)

class Catfish(Fish, Swimmer):
    def __init__(self, name):
        super().__init__(name, self.__class__.__name__)

class Otter(Fish, Walker, Swimmer):
    def __init__(self, name):
        super().__init__(name, self.__class__.__name__)

otter = Otter("John")
print(otter.swim())
print(otter.sound())

mouse = Mouse("Lilly")
print(mouse.walk())
print(mouse.sound())